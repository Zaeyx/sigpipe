""" Sigpipe.py is designed to be a simple signature searching utility.  In the future I would like to upgrade it to read text via natural language processing and detect emotional content.  For today, I'm writing the basic sections. """

import sys
import threading
import signal

signatures = ["example1","example2"]
threads = []

def signal_handler(signal, frame):
	print "Exiting..."
	sys.exit(0)

class thread_wrapper(threading.Thread):
	def __init__(self, node):
		threading.Thread.__init__(self)
		self.node = node

	def run(self):
		while True:
			tup = reading(self.node)
			if tup is not None:
				print "Node: %s has a hit: %s" % tup
def reading(node):
	fi = open(node,"r")
	while True:
		line = fi.readline()
		if detect(line) is not None:
			return (node, line)

def detect(line):
	for i in range(len(signatures)):
		if signatures[i] in line:
			return line
	return None
	
#Reading from node
if len(sys.argv) < 2:
	print "Specify a node created like so 'mknod backpipe p'"
	print "EX: %s backpipe" % repr(sys.argv[0])
	exit(1)

for i in range(len(sys.argv) - 1):
	threads.append(thread_wrapper(sys.argv[i+1]))

signal.signal(signal.SIGINT, signal_handler)
for thread in threads:
	thread.daemon = True
	thread.start()

signal.pause()


 

